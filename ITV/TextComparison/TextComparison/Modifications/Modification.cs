﻿namespace TextComparison.Modifications
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    using Collections;

    public abstract partial class Modification : OwnedItem, ICloneable
    {
        protected static Color DefaultColor = Color.Green; //.SystemColors.Window;
        protected static Color RemovedColor = Color.Red; //FromArgb(243, 230, 216);
        protected static Color ReplacedColor = Color.Red; //FromArgb( 230, 202, 172 );
        protected static Color AddedColor = Color.Orange; //FromArgb(209, 232, 207);
        protected static Color EmptyColor = Color.Black; //FromArgb(208, 220, 234);
        protected static Color MixedColor = Color.Black; //FromArgb(255, 208, 116);

        private const string NoChanged = "NoChanged";
        private const string Added = "Added";
        private const string Replaced = "Replaced";

        protected Modification(
            string name,
            IEnumerable< string > primaryLines,
            IEnumerable< string > secondaryLines,
            Color primaryColor,
            Color secondaryColor )
        {
            Name = name;
            Primary = new PrimarySection( this, primaryLines, primaryColor );
            Secondary = new SecondarySection( this, secondaryLines, secondaryColor );
        }

        public string Name
        {
            get;
        }

        public bool IsNoChanged => Name == NoChanged;

        public bool IsAdded => Name == Added;

        public bool IsReplaced => Name == Replaced;

        public Section Primary
        {
            get;
            protected set;
        }

        public Section Secondary
        {
            get;
            protected set;
        }

        public int Length => Math.Max( Primary.Length, Secondary.Length );

        public abstract object Clone();

        protected abstract Modification[] DoSplit( int primaryIndex );

        public override string ToString()
        {
            return $"{Name}, ({Primary.StartIndex}, {Primary.Length}, {Secondary.StartIndex}, {Secondary.Length})";
        }

        protected void SplitLines( IList< string > lines, int index, out string[] first, out string[] second )
        {
            IList< string > firstResult = new List< string >();

            for ( int lineIndex = 0; lineIndex < Math.Min( index, lines.Count ); lineIndex++ )
            {
                firstResult.Add( lines[ lineIndex ] );
            }

            first = firstResult.ToArray();

            IList< string > secondResult = new List< string >();

            for ( int lineIndex = index; lineIndex < lines.Count; lineIndex++ )
            {
                secondResult.Add( lines[ lineIndex ] );
            }

            second = secondResult.ToArray();
        }

        public Modification[] Split( int primaryIndex )
        {
            IList< Modification > result = new List< Modification >();

            if ( primaryIndex <= Primary.StartIndex || primaryIndex >= Primary.StartIndex + Primary.Length )
            {
                result.Add( this );

                return result.ToArray();
            }

            return DoSplit( primaryIndex );
        }
    }
}
