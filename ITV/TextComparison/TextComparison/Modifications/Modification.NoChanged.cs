﻿namespace TextComparison.Modifications
{
    using System.Collections.Generic;

    partial class Modification
    {
        private class NoChangedModification : Modification
        {
            public NoChangedModification( IEnumerable< string > lines )
                : base( NoChanged, lines, lines, DefaultColor, DefaultColor )
            {
            }

            protected override Modification[] DoSplit( int primaryIndex )
            {
                string[] firstLines;
                string[] secondLines;
                SplitLines( Primary.Lines, primaryIndex - Primary.StartIndex, out firstLines, out secondLines );

                return new Modification[]
                       {
                           new NoChangedModification( firstLines ),
                           new NoChangedModification( secondLines )
                       };
            }

            public override object Clone()
            {
                return new NoChangedModification( Primary.Lines );
            }
        }
    }
}
