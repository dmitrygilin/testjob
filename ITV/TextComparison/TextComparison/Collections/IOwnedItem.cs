﻿namespace TextComparison.Collections
{
    using System;

    public interface IOwnedItem
    {
        int Index
        {
            get;
        }

        IOwnedItem Previous
        {
            get;
        }

        IOwnedItem Next
        {
            get;
        }

        void SetOwner( IOwner ownerCollection );

        void Remove();

        event EventHandler Removed;
    }
}
