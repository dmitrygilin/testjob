﻿namespace TextComparison
{
    using System;

    public class TextLine : IComparable
    {
        private readonly int _hash;

        public TextLine( string line )
        {
            Line = line;
            _hash = CalculateHashCode( line );
        }

        public string Line
        {
            get;
        }

        public int CompareTo( object obj )
        {
            return _hash.CompareTo( ( (TextLine)obj )._hash );
        }

        private static int CalculateHashCode( string line )
        {
            int length;

            do
            {
                length = line.Length;
                line = line.Trim().Trim( '\t' );
            }
            while ( length != line.Length );

            return line.GetHashCode();
        }

        public override string ToString()
        {
            return Line;
        }
    }
}
