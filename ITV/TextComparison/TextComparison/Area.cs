namespace TextComparison
{
    using System.Collections.Generic;

    /// <summary>
    /// �������, ������������ �������������� � ���� ������ - Primary � Secondary
    /// </summary>
    public class Area
    {
        public Area( int primaryIndex, int secondaryIndex, int length )
        {
            PrimaryIndex = primaryIndex;
            SecondaryIndex = secondaryIndex;
            Length = length;
        }

        public static IComparer< Area > SecondaryIndexComparer
        {
            get
            {
                return new SecondaryComparer();
            }
        }

        public int PrimaryIndex
        {
            get;
        }

        public int SecondaryIndex
        {
            get;
        }

        public int Length
        {
            get;
            set;
        }

        private class SecondaryComparer : IComparer< Area >
        {
            public int Compare( Area x, Area y )
            {
                return x.SecondaryIndex.CompareTo( y.SecondaryIndex );
            }
        }
    }
}
