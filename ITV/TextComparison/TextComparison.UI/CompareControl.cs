﻿namespace TextComparison.UI
{
    using System;
    using System.Drawing;
    using System.IO;
    using System.Reflection;
    using System.Windows.Forms;

    using Modifications;

    public partial class CompareControl : UserControl
    {
        public CompareControl()
        {
            InitializeComponent();

            _primaryFile = new TextFile();
            _secondaryFile = new TextFile();
        }

        private readonly TextFile _primaryFile;
        private readonly TextFile _secondaryFile;

        public string PrimaryHeader
        {
            get
            {
                return primaryLabel.Text;
            }
            set
            {
                primaryLabel.Text = value;
            }
        }

        public string SecondaryHeader
        {
            get
            {
                return secondaryLabel.Text;
            }
            set
            {
                secondaryLabel.Text = value;
            }
        }

        public void Initialize( ModificationCollection modifications )
        {
            primaryListView.Items.Clear();
            secondaryListView.Items.Clear();
            primaryTextBox.Text = modifications.Primary.Name;
            secondaryTextBox.Text = modifications.Secondary.Name;

            int rowNumber = 1;

            foreach ( var modification in modifications )
            {
                for ( int index = 0; index < modification.Length; index++ )
                {
                    var primaryItem = new ListViewItem( rowNumber.ToString( "00000" ) );
                    primaryItem.BackColor = modification.Primary.Color;

                    string primaryLine = string.Empty;
                    if ( index < modification.Primary.Length )
                    {
                        primaryLine = modification.Primary.Lines[ index ];
                    }

                    primaryItem.SubItems.Add( primaryLine );
                    primaryListView.Items.Add( primaryItem );

                    var secondaryItem = new ListViewItem( rowNumber.ToString( "00000" ) );
                    secondaryItem.BackColor = modification.Secondary.Color;

                    string secondaryLine = string.Empty;
                    if ( index < modification.Secondary.Length )
                    {
                        secondaryLine = modification.Secondary.Lines[ index ];
                    }

                    secondaryItem.SubItems.Add( secondaryLine );
                    secondaryListView.Items.Add( secondaryItem );

                    rowNumber++;
                }
            }
        }

        private void CompareControlResize( object sender, EventArgs e )
        {
            int controlWidth = ( ClientRectangle.Width - splitter.Width ) / 2;

            primaryPanel.Width = controlWidth;
            secondaryPanel.Width = controlWidth;
        }

        private void PrimaryPanelResize( object sender, EventArgs e )
        {
            primaryListView.Columns[ 1 ].Width = primaryListView.ClientRectangle.Width - primaryListView.Columns[ 0 ].Width;
        }

        private void SecondaryPanelResize( object sender, EventArgs e )
        {
            secondaryListView.Columns[ 1 ].Width = secondaryListView.ClientRectangle.Width - secondaryListView.Columns[ 0 ].Width;
        }

        private static void SynchronizeCursor( ListView first, ListView second )
        {
            if ( first.SelectedIndices.Count > 0 )
            {
                int index = first.SelectedIndices[ 0 ];

                if ( index < second.Items.Count )
                {
                    second.SelectedIndices.Add( index );
                    second.EnsureVisible( index );
                }
            }
        }

        private void PrimaryListViewSelectedIndexChanged( object sender, EventArgs e )
        {
            SynchronizeCursor( primaryListView, secondaryListView );
        }

        private void SecondaryListViewSelectedIndexChanged( object sender, EventArgs e )
        {
            SynchronizeCursor( secondaryListView, primaryListView );
        }

        private void SelectPrimaryClick( object sender, EventArgs e )
        {
            string filename = GetFileName();

            if ( !File.Exists( filename ) )
            {
                return;
            }

            _primaryFile.Load( filename );
            ExecuteCompare();
            ExecuteCompare2();
        }

        private void SelectSecondaryClick( object sender, EventArgs e )
        {
            string filename = GetFileName();

            if ( !File.Exists( filename ) )
            {
                return;
            }

            _secondaryFile.Load( filename );
            ExecuteCompare();
            ExecuteCompare2();
        }

        private string GetFileName()
        {
            string location = Assembly.GetExecutingAssembly().Location;
            openFileDialog.InitialDirectory = location;
            openFileDialog.Filter = @"All files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            return openFileDialog.ShowDialog() == DialogResult.OK ? openFileDialog.FileName : null;
        }

        private void ExecuteCompare()
        {
            primaryListView.Items.Clear();
            primaryTextBox.Text = _primaryFile.Name;

            secondaryListView.Items.Clear();
            secondaryTextBox.Text = _secondaryFile.Name;

            FileComparer fileComparer = new FileComparer();

            ModificationCollection modifications = fileComparer.Compare( _primaryFile, _secondaryFile );

            int rowNumber = 1;

            foreach ( var modification in modifications )
            {
                for ( int index = 0; index < modification.Length; index++ )
                {
                    var primaryItem = PrepareItem( modification.Primary, index, rowNumber );
                    primaryListView.Items.Add( primaryItem );

                    var secondaryItem = PrepareItem( modification.Secondary, index, rowNumber );
                    secondaryListView.Items.Add( secondaryItem );

                    rowNumber++;
                }
            }
        }

        private static ListViewItem PrepareItem( Section section, int index, int rowNumber )
        {
            var primaryItem = new ListViewItem( rowNumber.ToString( "00000" ) )
                              {
                                  UseItemStyleForSubItems = false
                              };

            string primaryLine = string.Empty;
            if ( index < section.Length )
            {
                primaryLine = section.Lines[ index ];
            }

            primaryItem.SubItems.Add( new ListViewItem.ListViewSubItem
                                      {
                                          Text = primaryLine,
                                          ForeColor = section.Color
                                      } );
            return primaryItem;
        }

        /// <summary>
        /// Результат, требуемый заданием.
        /// </summary>
        private void ExecuteCompare2()
        {
            thirdListView.Items.Clear();

            FileComparer fileComparer = new FileComparer();

            ModificationCollection modifications = fileComparer.Compare( _primaryFile, _secondaryFile );

            //int rowNumber = 1;

            foreach ( var modification in modifications )
            {
                for ( int index = 0; index < modification.Length; index++ )
                {
                    string line;
                    Color color;
                    ListViewItem item;

                    if ( modification.IsReplaced )
                    {
                        line = modification.Primary.Lines[ index ];
                        color = modification.Primary.Color;

                        item = new ListViewItem( line ) { ForeColor = color };

                        thirdListView.Items.Add( item );

                        line = modification.Secondary.Lines[ index ];
                        color = modification.Secondary.Color;
                    }
                    else
                    {
                        if ( index < modification.Primary.Length )
                        {
                            line = modification.Primary.Lines[ index ];
                            color = modification.Primary.Color;
                        }
                        else
                        {
                            line = modification.Secondary.Lines[ index ];
                            color = modification.Secondary.Color;
                        }
                    }

                    item = new ListViewItem( line ) { ForeColor = color };

                    thirdListView.Items.Add( item );
                }
            }
        }

        private void ThirdListViewResize( object sender, EventArgs e )
        {
            thirdListView.Columns[ 0 ].Width = thirdListView.ClientRectangle.Width;
        }
    }
}
