﻿namespace TextComparisonTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using TextComparison;
    using TextComparison.Modifications;

    [ TestClass ]
    public class FileComparerTests
    {
        private static void ModificationAreEqual(
            Modification modification,
            string modificationName )
        {
            Assert.AreEqual( modification.Name, modificationName );
        }

        [ TestMethod ]
        public void TestAdded()
        {
            TextFile file1 = new TextFile();
            file1.AddLine( "1" );
            file1.AddLine( "3" );

            TextFile file2 = new TextFile();
            file2.AddLine( "1" );
            file2.AddLine( "2" );
            file2.AddLine( "3" );

            FileComparer fileComparer = new FileComparer();
            ModificationCollection report = fileComparer.Compare( file1, file2 );

            ModificationAreEqual( report[ 0 ], "NoChanged" );
            ModificationAreEqual( report[ 1 ], "Added" );
            ModificationAreEqual( report[ 2 ], "NoChanged" );
        }

        [ TestMethod ]
        public void TestReplaced()
        {
            TextFile file1 = new TextFile();
            file1.AddLine( "1" );
            file1.AddLine( "4" );
            file1.AddLine( "3" );

            TextFile file2 = new TextFile();
            file2.AddLine( "1" );
            file2.AddLine( "2" );
            file2.AddLine( "3" );

            FileComparer fileComparer = new FileComparer();
            ModificationCollection report = fileComparer.Compare( file1, file2 );

            ModificationAreEqual( report[ 0 ], "NoChanged" );
            ModificationAreEqual( report[ 1 ], "Replaced" );
            ModificationAreEqual( report[ 2 ], "NoChanged" );
        }

        [ TestMethod ]
        public void TestReplaced2()
        {
            TextFile file1 = new TextFile();
            file1.AddLine( "1" );
            file1.AddLine( "4" );
            file1.AddLine( "3" );

            TextFile file2 = new TextFile();
            file2.AddLine( "2" );

            FileComparer fileComparer = new FileComparer();
            ModificationCollection report = fileComparer.Compare( file1, file2 );

            ModificationAreEqual( report[ 0 ], "Replaced" );
            ModificationAreEqual( report[ 1 ], "Removed" );
        }

        [ TestMethod ]
        public void TestMethod1()
        {
            TextFile file1 = new TextFile();
            file1.AddLine( "public class MyClass" );
            file1.AddLine( "{" );
            file1.AddLine( "    public MyClass()" );
            file1.AddLine( "    { }" );
            file1.AddLine();
            file1.AddLine( "    public string StringProperty" );
            file1.AddLine( "    {" );
            file1.AddLine( "        get;" );
            file1.AddLine( "        set;" );
            file1.AddLine( "    }" );
            file1.AddLine( "}" );

            TextFile file2 = new TextFile();
            file2.AddLine( "public class MyClass" );
            file2.AddLine( "{" );
            file2.AddLine( "    public MyClass()" );
            file2.AddLine( "    { }" );
            file2.AddLine();
            file2.AddLine( "    public int MyMethod(int value)" );
            file2.AddLine( "    {" );
            file2.AddLine( "        Console.WriteLine(value);" );
            file2.AddLine( "    }" );
            file2.AddLine( "}" );

            FileComparer fileComparer = new FileComparer();
            ModificationCollection report = fileComparer.Compare( file1, file2 );

            ModificationAreEqual( report[ 0 ], "NoChanged" );
            ModificationAreEqual( report[ 1 ], "Replaced" );
            ModificationAreEqual( report[ 2 ], "NoChanged" );
            ModificationAreEqual( report[ 3 ], "Replaced" );
            ModificationAreEqual( report[ 4 ], "Removed" );
            ModificationAreEqual( report[ 5 ], "NoChanged" );
        }

        [ TestMethod ]
        public void TestEmptyFiles()
        {
            TextFile emptyFile = new TextFile();

            TextFile file2 = new TextFile();
            file2.AddLine( "public class MyClass" );
            file2.AddLine( "{" );
            file2.AddLine( "}" );

            FileComparer fileComparer = new FileComparer();

            ModificationCollection report = fileComparer.Compare( emptyFile, file2 );
            ModificationAreEqual( report[ 0 ], "Added" );

            report = fileComparer.Compare( file2, emptyFile );
            ModificationAreEqual( report[ 0 ], "Removed" );

            report = fileComparer.Compare( emptyFile, emptyFile );
            Assert.AreEqual( report.Count, 0 );
        }
    }
}
